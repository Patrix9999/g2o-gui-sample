#include "pch.h"

#include "GUI/CDraw.h"
#include "GUI/CDraw3d.h"
#include "GUI/CTexture.h"
#include "GUI/CLine2d.h"
#include "GUI/CLine3d.h"
#include "GUI/CItemRender.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	// Module Init
	sqModule::vm = vm;
	sqModule::api = api;

	sqModule::print = sq_getprintfunc(vm);
	sqModule::error = sq_geterrorfunc(vm);

	CTexture* red = new CTexture(0, 0, 4096, 4096, "RED");
	red->setVisible(true);

	CTexture* green = new CTexture(4096, 0, 4096, 4096, "GREEN");
	green->setVisible(true);

	CTexture* blue = new CTexture(0, 4096, 4096, 4096, "BLUE");
	blue->setVisible(true);

	CTexture* yellow = new CTexture(4096, 4096, 4096, 4096, "WHITE");
	yellow->setVisible(true);

	Sqrat::DefaultVM::Set(vm);

#ifdef __DEBUG
		if (
			AllocConsole() &&
			freopen("conin$", "r", stdin) &&
			freopen("conout$", "w", stdout) &&
			freopen("conout$", "w", stderr)
		)
			std::cout << "[Debug] Console initalized!\n";
#endif

	return SQ_OK;
}
